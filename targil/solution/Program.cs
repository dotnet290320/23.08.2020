﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _23Aug2020
{
    class Program
    {
        static void Main(string[] args)
        {
            Order o1 = new Order(100, 3);
            Order o2 = new Order(200, 2);

            //float sum = o1 + o2; //  ==> 300

            int x1 = 10;
            int y1 = 20;
            int z1 = x1 + y1;

            o1++; // add 1 to number of manot

            //Order CombinedOrder = o1 + o2; // ==> Order [total=300, num=5]
            o1.AddOrderToMine(o2);
            o2 = null;
            o1 = o2 + o2; // operator overloading

            o1 = o1 + 150;

            Order[] orders = new Order[]
            {
                new Order(200, 4),
                new Order(400, 7)
            };
            Array.Sort(orders); // will sort by number of manot
            Array.Sort(orders, new CompareOrderByTotal()); // will sort by total price
        }
    }
}
