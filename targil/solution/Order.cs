﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _23Aug2020
{
    class Order : IComparable
    {
        public float Total { get; set; }
        private int NumberOfManot { get; set; }
        public float Avg
        {
            get
            {
                return Total / NumberOfManot;
            }
        }

        public Order(float total, int numberOfManot)
        {
            Total = total;
            NumberOfManot = numberOfManot;
        }

        public static Order SumUpOrders(Order o1, Order o2)
        {
            return new Order(o1.Total + o2.Total, o1.NumberOfManot + o2.NumberOfManot);
        }
        public void AddOrderToMine(Order o2)
        {
            this.Total = this.Total + o2.Total;
            this.NumberOfManot = this.NumberOfManot + o2.NumberOfManot;
        }

        public static Order operator +(Order o1, Order o2)
        {
            if (o1 != null && o2 == null)
                return new Order(o1.Total, o1.NumberOfManot);
            if (o2 != null && o1 == null)
                return new Order(o2.Total, o2.NumberOfManot);
            if (o1 == null && o2 == null)
                return null;
            return new Order(o1.Total + o2.Total, o1.NumberOfManot + o2.NumberOfManot);
        }

        public static Order operator ++(Order o1)
        {
            return new Order(o1.Total, o1.NumberOfManot++);
        }

        public static Order operator +(Order o1, float cost)
        {
            return new Order(o1.Total + cost, o1.NumberOfManot + 1);
        }

        //public static float operator +(Order o1, Order o2)
        //{
        //    return o1.Total + o2.Total;
        //}


        public static Order operator -(Order o1, Order o2)
        {
            if (o1 != null && o2 == null)
                return new Order(o1.Total, o1.NumberOfManot);
            if (o2 != null && o1 == null)
                return new Order(o2.Total, o2.NumberOfManot);
            if (o1 == null && o2 == null)
                return null;
            return new Order(o1.Total - o2.Total, o1.NumberOfManot - o2.NumberOfManot);

        }

        public static Order operator *(Order o1, int x)
        {
            // check first
            return new Order(o1.Total * x, o1.NumberOfManot * x);
        }

        public static Order operator /(Order o1, int x)
        {
            // check first
            return new Order(o1.Total / x, o1.NumberOfManot / x);

        }

        public static bool operator ==(Order o1, Order o2)
        {
            if (ReferenceEquals(o1, null) && ReferenceEquals(o2, null))
            {
                return true;
            }

            if (ReferenceEquals(o1, null) || ReferenceEquals(o2, null))
            {
                return false;
            }

            if (o1.Total == o2.Total && o1.NumberOfManot == o2.NumberOfManot)
                return true;

            return false;
        }

        public static bool operator !=(Order o1, Order o2)
        {
            return !(o1 == o2);
        }

        public override string ToString()
        {
            return $"{base.ToString()} Total: {Total} NumberOfManot: {NumberOfManot}";
        }

        public override bool Equals(object obj)
        {
            Order otherOrder = obj as Order;

            return this == otherOrder;
        }

        // interface IComparable
        public int CompareTo(object obj)
        {
            // order by number of manot
            //if (obj == null)
               // return 1;

            //1
            //return this.NumberOfManot - ((Order)obj).NumberOfManot;
            //2
            //return this.NumberOfManot.CompareTo((obj as Order).NumberOfManot);
            //3
            Order otherOrder = obj as Order;
            if (this.NumberOfManot > otherOrder.NumberOfManot)
                return 1;
            if (this.NumberOfManot == otherOrder.NumberOfManot)
                return 0;
            return -1;

        }
    }

    class CompareOrderByTotal : IComparer
    {
        public int Compare(object x, object y)
        {
            // 1
            //return (x as Order).Total.CompareTo((y as Order).Total);

            // 2
            Order o1 = x as Order;
            Order o2 = y as Order;
            if (o1.Total > o2.Total)
                return 1;
            if (o1.Total == o2.Total)
                return 0;
            return -1;
        }
    }
}
